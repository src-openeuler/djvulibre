Name:             djvulibre
Summary:          An open source (GPL'ed) implementation of DjVu
Version:          3.5.28
Release:          1
License:          GPLv2+
URL:              http://djvu.sourceforge.net/
Source0:          http://downloads.sourceforge.net/djvu/djvulibre-%{version}.tar.gz
Patch0:           djvulibre-3.5.22-cdefs.patch
Patch1:           CVE-2021-32490.patch
Patch2:           CVE-2021-32491.patch
Patch3:           CVE-2021-32492.patch
Patch4:           CVE-2021-32493.patch
Patch5:           CVE-2021-3500.patch
Patch6:           CVE-2021-46310.patch
Patch7:           CVE-2021-46312.patch

Requires(post):   xdg-utils
Requires(preun):  xdg-utils
BuildRequires:    libjpeg-turbo-devel libtiff-devel xdg-utils chrpath hicolor-icon-theme gcc-c++
Provides:         djvulibre-mozplugin = %{version} djvulibre-libs = %{version}
Obsoletes:        djvulibre-mozplugin < 3.5.24 djvulibre-libs < %{version}
%description
DjVu is a set of compression technologies, a file format, and a software platform
for the deliveryover the Web of digital documents, scanned documents, and high
resolution images.DjVu documents download and display extremely quickly, and look
exactly the same on all platforms with no compatibility problems due to fonts,
colors, etc. DjVu can be seen as a superior alternative to PDF and PostScript for
digital documents, to TIFF (and PDF) for scanned bitonal documents, to JPEG and
JPEG2000 for photographs and pictures, and to GIF for large palettized images.
DjVu is the only Web format that is practical for distributing high-resolution
scanned documents in color.

%package devel
Summary:          Development files for DjVuLibre
Requires:         djvulibre = %{version}-%{release} pkgconfig

%description devel
Development files for DjVuLibre.

%package help
Summary:         Help documentation for the djvulibre

%description help
Help documentation for the djvulibre.

%prep
%autosetup -p1

%build
%configure --with-qt=%{_libdir}/qt-3.3 --enable-threads
%make_build V=1


%install
%make_install
find %{buildroot}%{_libdir} -name '*.so*' -exec chmod +x {} \;
for i in djvutoxml djvused cjb2 csepdjvu djvuserve djvm djvuxmlparser \
    djvutxt ddjvu djvumake cpaldjvu djvuextract c44 djvups djvudump djvmcvt bzz;
do
chrpath --delete $RPM_BUILD_ROOT%{_bindir}/$i;
done

rm -f $RPM_BUILD_ROOT%{_datadir}/mime/packages/djvulibre-mime.xml
cd desktopfiles
for i in 22 32 48 64 ; do
    mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/${i}x${i}/mimetypes/
    install ./prebuilt-hi${i}-djvu.png $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/${i}x${i}/mimetypes/image-vnd.djvu.mime.png
done
cd -
%delete_la


%post
rm -f %{_datadir}/applications/djvulibre-djview3.desktop || :
rm -f %{_datadir}/icons/hicolor/32x32/apps/djvulibre-djview3.png || :
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc COPYRIGHT COPYING
%{_bindir}/*
%{_datadir}/icons/*
%{_datadir}/djvu/
%{_libdir}/*.so.*

%files devel
%doc doc/*.*
%{_includedir}/libdjvu/
%{_libdir}/pkgconfig/ddjvuapi.pc
%{_libdir}/*.so

%files help
%doc README NEWS
%{_mandir}/man1/*

%changelog
* Mon Oct 16 2023 chenyaqiang <chenyaqiang@huawei.com> - 3.5.28-1
- Update to 3.5.28

* Wed Sep 13 2023 wangkai <13474090681@163.com> - 3.5.27-19
- Fix CVE-2021-46310,CVE-2021-46312

* Wed Jul 07 2021 wangyue<wangyue92@huawei.com> - 3.5.27-18
- Fix CVE-2021-3630

* Wed Jun 30 2021 liwu<liwu13@huawei.com> - 3.5.27-17
* Fix CVE-2021-32493 CVE-2021-3500

* Wed Jun 30 2021 liwu<liwu13@huawei.com> - 3.5.27-16
* Fix CVE-2021-32490, CVE-2021-32491, CVE-2021-32492

* Thu Jan 28 2021 lingsheng <lingsheng@huawei.com> - 3.5.27-15
- update any2djvu server hostname

* Mon Jan 4 2021 zhanghua <zhanghua40@huawei.com> - 3.5.27-14
- fix CVE-2019-15142, CVE-2019-15143, CVE-2019-15144, CVE-2019-15145, CVE-2019-18804

* Sun Sep 20 2020 yanan li <liyanan032@huawei.com> - 3.5.27-13
- delete %preun

* Fri Dec 20 2019 likexin <likexin4@huawei.com> - 3.5.27-12
- package init
